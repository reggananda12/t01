import javax.sound.sampled.SourceDataLine;

public class Main {
    public static class Mahasiswa {
        String nama;
        int nim;
        String prodi;
        int tahunmasuk;
        Transkrip[] transkrip;
    }

    public static class Kursus {
        String nama;
        int kodematkul;
        int sks;
    }

    public static class Transkrip {
        Mahasiswa mhs;
        Kursus kursus;
        int nilai;
    }

    public static void addTranscript(Mahasiswa mahasiswa, Transkrip transkrip, Transkrip[] transkrips) {
        Transkrip[] transkripBaru = new Transkrip[transkrips.length + 1];
        for (int i = 0; i < transkrips.length; i++) {
            transkripBaru[i] = transkrips[i];
        }
        transkripBaru[transkrips.length] = transkrip;
        mahasiswa.transkrip = transkripBaru;
    }

    public static void title(Mahasiswa mhs, Transkrip[] transkrips) {
        System.out.println("Transkrip nilai untuk " + mhs.nama + ":");
        for (Transkrip transcript : transkrips) {
            if (transcript != null && transcript.mhs == mhs) {
                cetakTranscript(transcript);
            }
        }
    }

    public static void cetakTranscript(Transkrip transcript) {
        System.out.println(transcript.kursus.nama + ": " + transcript.nilai);
    }

    public static void main(String[] args) {
        Mahasiswa mhs1 = new Mahasiswa();
        mhs1.nama = "Regga";
        mhs1.nim = 1234;
        mhs1.prodi = "Teknologi Informasi";
        mhs1.tahunmasuk = 2022;

        Mahasiswa mhs2 = new Mahasiswa();
        mhs2.nama = "Satrio";
        mhs2.nim = 12345;
        mhs2.prodi = "Sistem Informasi";
        mhs2.tahunmasuk = 2021;

        Kursus mk1 = new Kursus();
        mk1.nama = "Pemrograman Lanjut";
        mk1.kodematkul = 01;
        mk1.sks = 4;

        Kursus mk2 = new Kursus();
        mk2.nama = "Sistem Operasi";
        mk2.kodematkul = 02;
        mk2.sks = 3;

        Transkrip[] transcripts = new Transkrip[4];

        Transkrip t1 = new Transkrip();
        t1.mhs = mhs1;
        t1.kursus = mk1;
        t1.nilai = 100;
        transcripts[0]=t1;

        Transkrip t2 = new Transkrip();
        t2.mhs = mhs1;
        t2.kursus = mk2;
        t2.nilai = 90;
        transcripts[1]=t2;
      
        Transkrip t3 = new Transkrip();
        t3.mhs = mhs2;
        t3.kursus = mk1;
        t3.nilai = 100;
        transcripts[2]=t3;

        Transkrip t4 = new Transkrip();
        t4.mhs = mhs2;
        t4.kursus = mk2;
        t4.nilai = 90;
        transcripts[3]=t4;
        

        addTranscript(/*LENGKAPI CODE DISINI (5)*/mhs1, t1, transcripts);
        addTranscript(/*LENGKAPI CODE DISINI (2)*/mhs2, t2, transcripts);
        title(/*LENGKAPI CODE DISINI (3)*/mhs1, transcripts);
        addTranscript(/*LENGKAPI CODE DISINI (5)*/mhs1, t3, transcripts);
        addTranscript(/*LENGKAPI CODE DISINI (2)*/mhs2, t4, transcripts);
        title(/*LENGKAPI CODE DISINI (3)*/mhs2,transcripts);
    }
}